package com.donmutti.telegram.calendarbot;

import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.send.SendPhoto;
import org.telegram.telegrambots.api.objects.Chat;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.exceptions.TelegramApiRequestException;

import java.io.File;
import java.io.IOException;
import java.util.stream.Collectors;

public class CalendarBotApp extends TelegramLongPollingBot {

    // Bot info
    private static final String BOT_USERNAME = "GCalendarBot";
    private static final String BOT_TOKEN = "246406582:AAH8cM_1Apd_Qotba7csuvwnR7cXjQX3Rb0";
    private static final String BOT_DESCRIPTION = "I'm here to help you manage your Google Calendar events";

    private static final String CRLF = System.getProperty("line.separator");
    private static final String CRLF2 = CRLF + CRLF;

    // Commands
    private static final String COMMAND_CALENDARS = "/calendars";
    private static final String COMMAND_CONNECT = "/connect";
    private static final String COMMAND_HELP = "/help";
    private static final String COMMAND_DOYOULOVEME = "/doyouloveme";

    // Texts
    private static final String TEXT_UNKNOWN_COMMAND = "Sorry, I cannot recognize your command. Please take a look into [/help]";
    private static final String TEXT_START = "*Step 1/3. Connect me to your Google Calendar*" + CRLF +
            "1. First step" + CRLF +
            "2. Second step" + CRLF +
            "3. Third step";
    private static final String TEXT_HELP = BOT_DESCRIPTION + CRLF2 +
            "*Commands:*" + CRLF +
            "[/calendars] - list all your calendars" + CRLF +
            "[/connect] - connect to a calendar" + CRLF +
            "[/help] - show this help screen" + CRLF +
            "[/doyouloveme] - examine if I love you";

    // Emoji
    private static final String EMOJI_HEART = "\u2764"; // ❤

    public static void main(String[] args) {
        ApiContextInitializer.init();
        TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
        try {
            telegramBotsApi.registerBot(new CalendarBotApp());
        } catch (TelegramApiRequestException e) {
            e.printStackTrace();
        }
    }

    public final String getBotUsername() {
        return BOT_USERNAME;
    }

    @Override
    public final String getBotToken() {
        return BOT_TOKEN;
    }

    public void onUpdateReceived(Update update) {
        Message message = update.getMessage();

        try {
            if (message != null && message.hasText()) {
                Chat chat = message.getChat();
                switch (message.getText()) {

                    case COMMAND_CALENDARS:
                        respondCalendars(chat);
                        break;

                    case COMMAND_CONNECT:
                        respondConnect(chat);
                        break;

                    case COMMAND_HELP:
                        respondHelp(chat);
                        break;

                    case COMMAND_DOYOULOVEME:
                        respondDoYouLoveMe(chat);
                        break;

                    default:
                        respondUnknownCommand(chat);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private GoogleCalendarClient getClient(Long chatId) {
        return new GoogleCalendarClient();
    }

    private void respondCalendars(Chat chat) throws TelegramApiException {
        try {
            GoogleCalendarClient client = getClient(chat.getId());
            String text = client.getCalendars().stream()
                    .map(item -> item.getId() + CRLF + item.getSummary())
                    .collect(Collectors.joining(CRLF2));

            StringBuilder sb = new StringBuilder()
                    .append("*Available calendars:*").append(CRLF)
                    .append(text);

            sendText(chat, sb.toString());
        } catch (IOException e) {
            throw new TelegramApiException(e);
        }
    }

    private void respondConnect(Chat chat) {
        sendText(chat, TEXT_START);
    }

    private void respondHelp(Chat chat) {
        sendText(chat, TEXT_HELP);
    }

    private void respondDoYouLoveMe(Chat chat) {
        sendText(chat, String.format("Of course I love you, dear %s! " + EMOJI_HEART, chat.getFirstName()));
    }

    private void respondUnknownCommand(Chat chat) {
        sendText(chat, TEXT_UNKNOWN_COMMAND);
    }

    private void sendPhoto(Chat chat, String fileName, String caption) {
        try {
            SendPhoto sendPhoto = new SendPhoto();
            sendPhoto.setNewPhoto(new File(getClass().getClassLoader().getResource(fileName).getFile()));
            sendPhoto.setChatId(chat.getId());
            sendPhoto.setCaption(caption);
            sendPhoto(sendPhoto);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private void sendText(Chat chat, String text) {
        SendMessage message = new SendMessage();
        message.enableMarkdown(true);
        message.setChatId(chat.getId());
        message.enableMarkdown(true);
        message.setText(text);
        try {
            sendMessage(message);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private void sendReply(Chat chat, Integer messageId, String text) {
        SendMessage replyMessage = new SendMessage();
        replyMessage.enableMarkdown(true);
        replyMessage.setChatId(chat.getId());
        replyMessage.setReplyToMessageId(messageId);
        replyMessage.setText(text);
        try {
            sendMessage(replyMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}
